// Google Authenticator
// Copyright (C) 2014 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _CACHE_H
#define _CACHE_H

struct cache_entry
{
    uint8_t account[SHA1_HASH_LENGTH];
    uint8_t password[SHA1_HASH_LENGTH];
    time_t age;
};

typedef std::map<std::string, struct cache_entry *> cache_map;

class Cache
{
public:
    Cache() : modified(0), fd(-1) { }
    virtual ~Cache() { Close(); }

    void Open(const string &filename);
    void Close(void);

    int Lookup(const string &account, struct cache_entry *entry);
    void Update(const string &account, const string &password);
    void Delete(const string &account);

protected:
    cache_map cache;
    time_t modified;
    int fd;

    void ReadLock(void);
    void WriteLock(void);
    void Unlock(void);

    void Read(void);
    void Write(void);

    void HashToString(uint8_t *hash, string &dest);
};
    
#endif // _CACHE_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
