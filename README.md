Google Authenticator for PAM
============================

This is a PAM module which authenticates a user using their Google credentials.

The module performs authentication using the GDATA protocol (HTTPS).

Module Options
--------------

The module accepts three arguments:

* domain
  Google-hosted domain name.  Default: gmail.com
* max_age
  Number of seconds to hold a cached login.  Default: 3600
* cache_file
  Location of cache file.  Default: /var/state/pam-google-auth/auth-cache.dat


