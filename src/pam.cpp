// Google Authenticator
// Copyright (C) 2014 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define PAM_SM_ACCOUNT
#define PAM_SM_AUTH
#define PAM_SM_PASSWORD
#define PAM_SM_SESSION

#include <stdexcept>
#include <iostream>
#include <map>

#include <sys/types.h>

#include <stdint.h>
#include <syslog.h>
#include <pwd.h>

#include <security/pam_appl.h>
#include <security/pam_modules.h>

using std::map;
using std::string;
using std::runtime_error;

extern "C" {
#include "sha1.h"
}

#include "cache.h"
#include "service.h"

typedef std::map<std::string, std::string> option;

// PAM argument parser
void pam_parse_arguments(int argc, const char **argv, option &options)
{
    size_t pos;
    string key, value;

    for (int i = 0; i < argc; i++) {

        key = argv[i];
        pos = key.find('=');

        if (pos != std::string::npos) {
            value = key.substr(pos + 1);
            key = key.substr(0, pos);
        }
        else value.clear();

        options[key] = value;

        syslog(LOG_AUTH | LOG_DEBUG, "%s:%d, key: \"%s\", value: \"%s\"",
            __PRETTY_FUNCTION__, __LINE__, key.c_str(), value.c_str());
    }
}

// PAM entry point for session creation
PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d: ignored", __PRETTY_FUNCTION__, __LINE__);
    return(PAM_IGNORE);
}

// PAM entry point for session cleanup
PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d: ignored", __PRETTY_FUNCTION__, __LINE__);
    return(PAM_IGNORE);
}

// PAM entry point for accounting
PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d: ignored", __PRETTY_FUNCTION__, __LINE__);
    return(PAM_IGNORE);
}

// PAM entry point for authentication verification
PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    int rc;
    char *username, *password;
    struct pam_message msg[1], *pmsg[1];
    struct pam_response *resp = NULL;
    struct pam_conv *conv;

    sha1 sha1_ctx;

    try {
        Cache cache;
        int cache_valid = 0;
        struct cache_entry entry;

        syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

        // Set default options
        option options;
        options["domain"] = "gmail.com";
        options["max_age"] = "3600";
        options["cache_file"] = "/var/state/pam-google-auth/auth-cache.dat";

        pam_parse_arguments(argc, argv, options);

        pmsg[0] = &msg[0];
        msg[0].msg_style = PAM_PROMPT_ECHO_OFF;
        msg[0].msg = "Google Password: ";

        if((rc = pam_get_item(pamh, PAM_USER, (const void **)&username)) != PAM_SUCCESS) {
            syslog(LOG_AUTH | LOG_DEBUG,
                "%s:%d: get user failed", __PRETTY_FUNCTION__, __LINE__);
            return rc;
        }

        struct passwd *pwent = getpwnam(username);
        if (pwent == NULL) {
            syslog(LOG_AUTH | LOG_DEBUG,
                "%s:%d: user not found (getpwnam): %s",
                __PRETTY_FUNCTION__, __LINE__, username);
            return(PAM_AUTH_ERR);
        }

        if(options.find("try_first_pass") != options.end() &&
            (rc = pam_get_item(pamh, PAM_AUTHTOK, (const void **)&password)) == PAM_SUCCESS) {
            // XXX: Enable only for test builds!
            //syslog(LOG_AUTH | LOG_DEBUG,
            //    "%s:%d: Using first password: %s", __PRETTY_FUNCTION__, __LINE__, password);
        }
        else {
            if ((rc = pam_get_item(pamh, PAM_CONV, (const void **)&conv)) != PAM_SUCCESS) {
                syslog(LOG_AUTH | LOG_ERR,
                    "%s:%d: get PAM conversation failed", __PRETTY_FUNCTION__, __LINE__);
                return rc;
            }

            if ((rc = conv->conv(1, (const struct pam_message **)pmsg,
                &resp, conv->appdata_ptr)) != PAM_SUCCESS) {
                syslog(LOG_AUTH | LOG_ERR,
                    "%s:%d: PAM conversation failed", __PRETTY_FUNCTION__, __LINE__);
                return rc;
            }

            if (resp != NULL) {
                if((flags & PAM_DISALLOW_NULL_AUTHTOK) && resp[0].resp == NULL ) {
                    return PAM_CONV_ERR;
                }

                password = resp[0].resp;
                resp[0].resp = NULL;                          
            }
            else {
                syslog(LOG_AUTH | LOG_DEBUG,
                    "%s:%d: NULL response", __PRETTY_FUNCTION__, __LINE__);
                return PAM_CONV_ERR;
            }
        }

        try {
            cache.Open(options["cache_file"]);
            cache_valid = cache.Lookup(username, &entry);
            syslog(LOG_AUTH | LOG_DEBUG,
                "%s:%d: cache valid: %d", __PRETTY_FUNCTION__, __LINE__, cache_valid);
        }
        catch (runtime_error &e) {
            syslog(LOG_AUTH | LOG_DEBUG,
                "%s:%d: cache lookup failed: %s", __PRETTY_FUNCTION__, __LINE__, e.what());
        }

        string account(username);
        account.append("@");
        account.append(options["domain"]);

        if (cache_valid) {
            time_t max_age = (time_t)atoi(options["max_age"].c_str());
            if (time(NULL) - entry.age < max_age) {
                sha1_init(&sha1_ctx);
                sha1_write(&sha1_ctx, password, strlen(password));
                uint8_t *hash = sha1_result(&sha1_ctx);

                if (!memcmp(hash, entry.password, SHA1_HASH_LENGTH)) {
                    syslog(LOG_AUTH | LOG_DEBUG,
                        "%s:%d: authorized (%s via cache)",
                        __PRETTY_FUNCTION__, __LINE__, account.c_str());
                    return PAM_SUCCESS;
                }
            }
        }

        gdata::client::Service service("apps", "ClearOS-PAM-Plugin");

        try {
            service.ClientLogin(account, password);

            syslog(LOG_AUTH | LOG_INFO,
                "%s:%d: authorized (%s via google)",
                __PRETTY_FUNCTION__, __LINE__, account.c_str());

            try {
                cache.Update(username, password);
            }
            catch (runtime_error &e) {
                syslog(LOG_AUTH | LOG_ERR, "%s:%d: cache update failed: %s",
                    __PRETTY_FUNCTION__, __LINE__, e.what());
            }

            return PAM_SUCCESS;

        } catch (runtime_error &e) {
            syslog(LOG_AUTH | LOG_ERR, "%s:%d: authorization failure (%s)",
                __PRETTY_FUNCTION__, __LINE__, account.c_str());
            syslog(LOG_AUTH | LOG_DEBUG, "%s:%d: %s",
                __PRETTY_FUNCTION__, __LINE__, e.what());
        }

        try {
            cache.Delete(username);
        }
        catch (runtime_error &e) {
            syslog(LOG_AUTH | LOG_ERR, "%s:%d: cache delete failed: %s",
                __PRETTY_FUNCTION__, __LINE__, e.what());
        }

    } catch(std::exception &e) {
        syslog(LOG_AUTH | LOG_ERR,
            "%s:%d: authorization exception: %s",
            __PRETTY_FUNCTION__, __LINE__, e.what());
    }

    return(PAM_AUTH_ERR);
}

// PAM entry point for setting user credentials (that is, to actually
// establish the authenticated user's credentials to the service provider)
PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d: ignored", __PRETTY_FUNCTION__, __LINE__);
    return(PAM_IGNORE);
}

// PAM entry point for authentication token (password) changes
PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d: ignored", __PRETTY_FUNCTION__, __LINE__);
    return(PAM_IGNORE);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
