// Google Authenticator
// Copyright (C) 2014 ClearFoundation <http://www.clearfoundation.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdexcept>
#include <map>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>

using std::map;
using std::string;
using std::runtime_error;

extern "C" {
#include "sha1.h"
}
#include "cache.h"

void Cache::Open(const string &filename)
{
    int flags = O_RDWR | O_NOFOLLOW;
    struct stat cache_stat;

    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    Close();

    if (stat(filename.c_str(), &cache_stat) != 0)
        flags |= O_CREAT;

    if ((fd = open(filename.c_str(), flags, S_IRUSR | S_IWUSR)) < 0) {
        syslog(LOG_AUTH | LOG_ERR, "%s:%d: open: %s: %s",
            __PRETTY_FUNCTION__, __LINE__, filename.c_str(), strerror(errno));
        throw runtime_error(strerror(errno));
    }

    Read();
}

void Cache::Close(void)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    if (fd != -1) { close(fd); fd = -1; }
    for (cache_map::iterator i = cache.begin(); i != cache.end(); i++)
        delete i->second;
    cache.clear();
    modified = 0;
}

int Cache::Lookup(const string &account, struct cache_entry *entry)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    sha1 sha1_ctx;
    sha1_init(&sha1_ctx);
    sha1_write(&sha1_ctx, account.c_str(), account.length());

    Read();

    string key;
    HashToString(sha1_result(&sha1_ctx), key);

    cache_map::iterator i = cache.find(key);

    if (i == cache.end()) return 0;

    memcpy(entry, i->second, sizeof(struct cache_entry));

    return 1;
}

void Cache::Update(const string &account, const string &password)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    try {
        Read();
        WriteLock();

        sha1 sha1_ctx;
        sha1_init(&sha1_ctx);
        sha1_write(&sha1_ctx, account.c_str(), account.length());
        uint8_t *account_hash = sha1_result(&sha1_ctx);

        string key;
        HashToString(account_hash, key);

        cache_map::iterator i = cache.find(key);

        struct cache_entry *entry;

        if (i == cache.end()) {
            entry = new struct cache_entry;
            cache[key] = entry;
        }
        else
            entry = i->second;

        memcpy(entry->account, account_hash, SHA1_HASH_LENGTH);

        sha1_init(&sha1_ctx);
        sha1_write(&sha1_ctx, password.c_str(), password.length());
        uint8_t *password_hash = sha1_result(&sha1_ctx);

        memcpy(entry->password, password_hash, SHA1_HASH_LENGTH);

        entry->age = time(NULL);

        Write();
    }
    catch (runtime_error &e) {
        Unlock();
        throw;
    }

    Unlock();
}

void Cache::Delete(const string &account)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    try {
        Read();
        WriteLock();

        sha1 sha1_ctx;
        sha1_init(&sha1_ctx);
        sha1_write(&sha1_ctx, account.c_str(), account.length());

        string key;
        HashToString(sha1_result(&sha1_ctx), key);

        cache_map::iterator i = cache.find(key);

        if (i == cache.end()) {
            Unlock();
            return;
        }

        delete i->second;
        cache.erase(i);

        Write();
    }
    catch (runtime_error &e) {
        Unlock();
        throw;
    }

    Unlock();
}

void Cache::ReadLock(void)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    if (flock(fd, LOCK_SH) != 0) {
        syslog(LOG_AUTH | LOG_ERR, "%s:%d: flock: %s",
            __PRETTY_FUNCTION__, __LINE__, strerror(errno));
        throw runtime_error(strerror(errno));
    }
}

void Cache::WriteLock(void)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    if (flock(fd, LOCK_EX) != 0) {
        syslog(LOG_AUTH | LOG_ERR, "%s:%d: flock: %s",
            __PRETTY_FUNCTION__, __LINE__, strerror(errno));
        throw runtime_error(strerror(errno));
    }
}

void Cache::Unlock(void)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);
    if (flock(fd, LOCK_UN) != 0) {
        syslog(LOG_AUTH | LOG_ERR, "%s:%d: flock: %s",
            __PRETTY_FUNCTION__, __LINE__, strerror(errno));
    }
}

void Cache::Read(void)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    ssize_t bytes;
    struct stat cache_stat;
    struct cache_entry buffer;

    ReadLock();

    if (fstat(fd, &cache_stat) != 0) {
        Unlock();
        throw runtime_error(strerror(errno));
    }

    if (cache_stat.st_mtime <= modified) {
        Unlock();
        return;
    }

    lseek(fd, 0L, SEEK_SET);

    do {
        bytes = read(fd, (void *)&buffer, sizeof(struct cache_entry));
        if (bytes == sizeof(struct cache_entry)) {
            string key;
            HashToString(buffer.account, key);

            struct cache_entry *entry = new struct cache_entry;
            memcpy(entry, &buffer, sizeof(struct cache_entry));

            cache_map::iterator i = cache.find(key);
            if (i != cache.end()) {
                delete i->second;
                cache.erase(i);
            }
            cache[key] = entry;
        }
    } while (bytes > 0);

    modified = cache_stat.st_mtime;

    Unlock();
}

void Cache::Write(void)
{
    syslog(LOG_AUTH | LOG_DEBUG, "%s:%d", __PRETTY_FUNCTION__, __LINE__);

    lseek(fd, 0L, SEEK_SET);
    if (ftruncate(fd, 0L) != 0) {
        syslog(LOG_AUTH | LOG_WARNING, "%s:%d: ftruncate: %s",
            __PRETTY_FUNCTION__, __LINE__, strerror(errno));
    }

    for (cache_map::iterator i = cache.begin(); i != cache.end(); i++) {
        if (write(fd, (void *)i->second,
            sizeof(struct cache_entry)) != sizeof(struct cache_entry)) {
            throw runtime_error(strerror(errno));
        }
    }

    struct stat cache_stat;

    if (fstat(fd, &cache_stat) != 0)
        throw runtime_error(strerror(errno));

    modified = cache_stat.st_mtime;
}

void Cache::HashToString(uint8_t *hash, string &dest)
{
    char buffer[SHA1_HASH_LENGTH * 2 + 1];
    char *p = buffer;

    memset(buffer, 0, SHA1_HASH_LENGTH * 2 + 1);
    for (int i = 0; i < SHA1_HASH_LENGTH; i++, p += 2)
        sprintf(p, "%02x", hash[i]);

    dest = buffer;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
